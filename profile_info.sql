SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

-- Database: `profile_info

-- Table structure for table `profile`
CREATE TABLE `profile` (
  `Id` int UNSIGNED NOT NULL,
  `First_Name` varchar(15) NOT NULL,
  `Middle_Name` varchar(15) DEFAULT NULL,
  `Last_Name` varchar(15) NOT NULL,
  `Date_of_Birth` date NOT NULL,
  `Gender` varchar(7) NOT NULL,
  `Profile_Pic` mediumblob,
  `Email_Id` varchar(30) NOT NULL,
  `Contact_Number` bigint NOT NULL,
  `Address` text,
  `Professional_Links` varchar(30) DEFAULT NULL,
  `Educational_Qualifications` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `About` text,
  `UpdationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- Table structure for table `Skills_info`
CREATE TABLE `Skills_info` (
  `Id` int UNSIGNED NOT NULL,
  `Skills` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- Table structure for table `Interests_info`
CREATE TABLE `Interests_info` (
  `Id` int UNSIGNED NOT NULL,
  `Interests` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Indexes for table `Interests_info`
ALTER TABLE `Interests_info`
  ADD PRIMARY KEY (`Id`,`Interests`);

-- Indexes for table `profile`
ALTER TABLE `profile`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Email_Id` (`Email_Id`),
  ADD UNIQUE KEY `Contact_Number` (`Contact_Number`),
  ADD UNIQUE KEY `Educational_Qualifications` (`Educational_Qualifications`);


-- Indexes for table `Skills_info`
ALTER TABLE `Skills_info`
  ADD PRIMARY KEY (`Id`,`Skills`);


-- AUTO_INCREMENT for table `profile`
ALTER TABLE `profile`
  MODIFY `Id` int UNSIGNED NOT NULL AUTO_INCREMENT;

-- Constraints for table `Interests_info`
ALTER TABLE `Interests_info`
  ADD CONSTRAINT `Interests_info_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `profile` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Constraints for table `Skills_info`
ALTER TABLE `Skills_info`
  ADD CONSTRAINT `Skills_info_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `profile` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;
