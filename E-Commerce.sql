SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

-- Database: `E-Commerce`

-- Table structure for table `Customer_info`
CREATE TABLE `Customer_info` (
  `Cust_Id` int UNSIGNED NOT NULL,
  `Cust_Name` varchar(40) NOT NULL,
  `Gender` enum('M','F') NOT NULL,
  `Email` varchar(40) NOT NULL,
  `Phone` bigint NOT NULL,
  `Cust_Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Table structure for table `Order_info`
CREATE TABLE `Order_info` (
  `Order_Id` int UNSIGNED NOT NULL,
  `Prod_Id` int UNSIGNED NOT NULL,
  `Quantity` int NOT NULL,
  `Amount` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Table structure for table `Customer_Order_info`
CREATE TABLE `Customer_Order_info` (
  `Order_Id` int UNSIGNED NOT NULL,
  `Cust_Id` int UNSIGNED NOT NULL,
  `Order_date` date NOT NULL,
  `Order_Status` varchar(15) NOT NULL,
  `Total_Amount` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Table structure for table `Product_info`
CREATE TABLE `Product_info` (
  `Prod_Id` int UNSIGNED NOT NULL,
  `Prod_Name` varchar(30) NOT NULL,
  `Price` double NOT NULL,
  `Description` varchar(100) DEFAULT NULL,
  `Image` mediumblob,
  `Prod_Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Indexes for table `Customer_info`
ALTER TABLE `Customer_info`
  ADD PRIMARY KEY (`Cust_Id`),
  ADD UNIQUE KEY `Email` (`Email`),
  ADD UNIQUE KEY `Phone` (`Phone`),
  ADD KEY `Cust_Id` (`Cust_Id`);

-- Indexes for table `Customer_Order_info`
ALTER TABLE `Customer_Order_info`
  ADD PRIMARY KEY (`Order_Id`),
  ADD KEY `Order_Id` (`Order_Id`),
  ADD KEY `Cust_Id` (`Cust_Id`);

-- Indexes for table `Order_info`
ALTER TABLE `Order_info`
  ADD PRIMARY KEY (`Order_Id`,`Prod_Id`),
  ADD KEY `Order_Id` (`Order_Id`),
  ADD KEY `Prod_Id` (`Prod_Id`);

-- Indexes for table `Product_info`
ALTER TABLE `Product_info`
  ADD PRIMARY KEY (`Prod_Id`),
  ADD KEY `Prod_Id` (`Prod_Id`),
  ADD KEY `Prod_Id_2` (`Prod_Id`);

-- AUTO_INCREMENT for table `Customer_info`
ALTER TABLE `Customer_info`
  MODIFY `Cust_Id` int UNSIGNED NOT NULL AUTO_INCREMENT;

-- AUTO_INCREMENT for table `Customer_Order_info`
ALTER TABLE `Customer_Order_info`
  MODIFY `Order_Id` int UNSIGNED NOT NULL AUTO_INCREMENT;

-- AUTO_INCREMENT for table `Product_info`
ALTER TABLE `Product_info`
  MODIFY `Prod_Id` int UNSIGNED NOT NULL AUTO_INCREMENT;

-- Constraints for table `Customer_Order_info`
ALTER TABLE `Customer_Order_info`
  ADD CONSTRAINT `Customer_Order_info_ibfk_1` FOREIGN KEY (`Cust_Id`) REFERENCES `Customer_info` (`Cust_Id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Constraints for table `Order_info`
ALTER TABLE `Order_info`
  ADD CONSTRAINT `Order_info_ibfk_1` FOREIGN KEY (`Order_Id`) REFERENCES `Customer_Order_info` (`Order_Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Order_info_ibfk_2` FOREIGN KEY (`Prod_Id`) REFERENCES `Product_info` (`Prod_Id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;
