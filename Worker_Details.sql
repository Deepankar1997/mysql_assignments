SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

-- Database: `Worker_Details`

-- Table structure for table `Worker`
CREATE TABLE `Worker` (
  `Worker_id` int UNSIGNED NOT NULL,
  `First_Name` varchar(20) NOT NULL,
  `Last_Name` varchar(20) DEFAULT NULL,
  `Date_of_Birth` date NOT NULL,
  `Gender` enum('M','F') NOT NULL,
  `Hire_date` date NOT NULL,
  `Designation` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Table structure for table `Department`
CREATE TABLE `Department` (
  `Dept_id` int UNSIGNED NOT NULL,
  `Dept_name` int NOT NULL,
  `In-Charge_worker_id` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Table structure for table `Emp_Dept`
CREATE TABLE `Emp_Dept` (
  `Worker_id` int UNSIGNED NOT NULL,
  `Dept_id` int UNSIGNED NOT NULL,
  `From_Date` date NOT NULL,
  `To_Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Table structure for table `Salary`
CREATE TABLE `Salary` (
  `Worker_id` int UNSIGNED NOT NULL,
  `From_Date` date NOT NULL,
  `To_Date` date NOT NULL,
  `Salary_amount` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Indexes for table `Department`
ALTER TABLE `Department`
  ADD PRIMARY KEY (`Dept_id`),
  ADD KEY `Dept_id` (`Dept_id`),
  ADD KEY `In-Charge_worker_id` (`In-Charge_worker_id`);

-- Indexes for table `Emp_Dept`
ALTER TABLE `Emp_Dept`
  ADD PRIMARY KEY (`Worker_id`,`Dept_id`),
  ADD KEY `Worker_id` (`Worker_id`),
  ADD KEY `Dept_id` (`Dept_id`);

-- Indexes for table `Salary`
ALTER TABLE `Salary`
  ADD PRIMARY KEY (`Worker_id`,`From_Date`),
  ADD KEY `Worker_id` (`Worker_id`);

-- Indexes for table `Worker`
ALTER TABLE `Worker`
  ADD PRIMARY KEY (`Worker_id`),
  ADD KEY `Worker_id` (`Worker_id`);

-- AUTO_INCREMENT for table `Worker`
ALTER TABLE `Worker`
  MODIFY `Worker_id` int UNSIGNED NOT NULL AUTO_INCREMENT;

-- Constraints for table `Department`
ALTER TABLE `Department`
  ADD CONSTRAINT `Department_ibfk_1` FOREIGN KEY (`In-Charge_worker_id`) REFERENCES `Worker` (`Worker_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Constraints for table `Emp_Dept`
ALTER TABLE `Emp_Dept`
  ADD CONSTRAINT `Emp_Dept_ibfk_1` FOREIGN KEY (`Worker_id`) REFERENCES `Worker` (`Worker_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Emp_Dept_ibfk_2` FOREIGN KEY (`Dept_id`) REFERENCES `Department` (`Dept_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Constraints for table `Salary`
ALTER TABLE `Salary`
  ADD CONSTRAINT `Salary_ibfk_2` FOREIGN KEY (`Worker_id`) REFERENCES `Worker` (`Worker_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;
